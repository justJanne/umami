FROM node:alpine
RUN apk add --no-cache git && \
    git clone https://github.com/mikecao/umami.git /umami
WORKDIR /umami
ADD src/fix.diff /umami/fix.diff
RUN git apply fix.diff && \
    npm install && \
    npm run build-tracker && \
    DATABASE_URL=postgresql://localhost/db npm run build
WORKDIR /umami
ENTRYPOINT ["npm", "start"]
